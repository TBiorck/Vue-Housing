import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import AgentProfile from '../views/AgentProfile.vue'
import HouseView from '../views/HouseView.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/agent/:id',
    name: 'Agent',
    component: AgentProfile
  },
  {
    path: '/house',
    name: 'House',
    component: HouseView,
    props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
